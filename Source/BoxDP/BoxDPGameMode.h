// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BoxDPGameMode.generated.h"

UCLASS(minimalapi)
class ABoxDPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABoxDPGameMode();
};



