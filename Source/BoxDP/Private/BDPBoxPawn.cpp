// BoxDPTestTask


#include "BDPBoxPawn.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Player/BDPCharacter.h"

// Sets default values
ABDPBoxPawn::ABDPBoxPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->OnComponentHit.AddDynamic(this, &ABDPBoxPawn::OnHit);
	
	RootComponent = StaticMesh;
	//StaticMesh->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void ABDPBoxPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABDPBoxPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	this->AddMovementInput(GetActorForwardVector(), 5.0f);
}

// Called to bind functionality to input
void ABDPBoxPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABDPBoxPawn::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ABDPCharacter* MyChar = Cast<ABDPCharacter>(OtherActor);

	if (CanPush())
	{
		if (MyChar->GetIsPushing())
		{
			UE_LOG(LogTemp, Warning, TEXT("Ok, u r pushing!"));
			SetBoxPush(true);
		

		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("U can push, but u r not pushing"));
			SetBoxPush(false);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("U Can't Push, try to face the box"));
		SetBoxPush(false);
	}
}

bool ABDPBoxPawn::CanPush()
{
	FHitResult Hit;
	FHitResult OutHit;

	auto Char = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	
	
	FVector CharLocation = Char->GetActorLocation();//GetWorld()->GetPlayerCharacter()->GetPawn()->GetActorLocation();
	FVector CharForwardVector = Char->GetActorForwardVector() * 100.0f;//GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorForwardVector() * 100.0f;
	FVector EndLocation = CharLocation + CharForwardVector;
	FCollisionQueryParams TraceParams;

	bool bHit = GetWorld()->LineTraceSingleByChannel(OutHit, CharLocation, EndLocation, ECC_Visibility, TraceParams);

	DrawDebugLine(GetWorld(), CharLocation , EndLocation, FColor::Red);

	if (bHit)
	{
		if (OutHit.Actor == this)
		{
	
			return true;
		}
		else
		{
			UE_LOG(LogTemp, Display, TEXT("just bad"))
			return false;
		}
	}
	else
	{
		UE_LOG(LogTemp, Display, TEXT("so bad"))
		return false;
	}
}

void ABDPBoxPawn::MoveBoxForward(float& Value, FVector& CharacterVector)
{
	FVector Location = GetActorLocation();
	this->AddMovementInput(Location + CharacterVector, Value);
	UE_LOG(LogTemp,Display, TEXT("Push Fwd"))
}

void ABDPBoxPawn::MoveBoxRight(float& Value, FVector& CharacterVector)
{
	FVector Location = GetActorLocation();
	this->AddMovementInput(Location + CharacterVector, Value);
	UE_LOG(LogTemp,Display, TEXT("Push Right"))
}

