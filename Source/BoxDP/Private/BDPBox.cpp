// BoxDPTestTask
#include "BDPBox.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/EngineTypes.h"
#include "Player/BDPCharacter.h"

// Sets default values
ABDPBox::ABDPBox()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->OnComponentHit.AddDynamic(this, &ABDPBox::OnHit);
	StaticMesh->SetupAttachment(GetRootComponent());
	
}

// Called when the game starts or when spawned
void ABDPBox::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABDPBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

void ABDPBox::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//UE_LOG(LogTemp, Warning, TEXT("Hit"));

	FAttachmentTransformRules AttachmentRules = FAttachmentTransformRules
	   (EAttachmentRule::KeepWorld,
		EAttachmentRule::KeepWorld,
		EAttachmentRule::KeepWorld,
		false);

	ABDPCharacter* MyChar = Cast<ABDPCharacter>(OtherActor);

	if (CanPush())
	{
		if (MyChar->GetIsPushing())
		{
			UE_LOG(LogTemp, Warning, TEXT("Ok, u r pushing!"));
			SetBoxPush(true);
		//	MyChar->OnMovingCharForward.AddDynamic(this, &ABDPBox::MoveBoxForward);
		//	MyChar->OnMovingCharRight.AddDynamic(this, &ABDPBox::MoveBoxRight);
			AttachToActor(MyChar, AttachmentRules);

		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("U can push, but u r not pushing"));
			SetBoxPush(false);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("U Can't Push, try to face the box"));
		SetBoxPush(false);
	}
}

bool ABDPBox::CanPush()
{

	FHitResult Hit;
	FHitResult OutHit;
	FVector CharLocation = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	FVector CharForwardVector = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorForwardVector() * 100.0f;
	FVector EndLocation = CharLocation + CharForwardVector;
	FCollisionQueryParams TraceParams;

	bool bHit = GetWorld()->LineTraceSingleByChannel(OutHit, CharLocation, EndLocation, ECC_Visibility, TraceParams);

	if (bHit)
	{
		if (OutHit.Actor == this)
		{
			return true;
		}
		else return false;
	}
	else return false;
}

void ABDPBox::MoveBoxForward(float& Value, FVector& CharacterVector)
{
	if (GetBoxPush())
	{

		FVector Location = GetActorLocation();
		SetActorLocation(Location + CharacterVector * Value, true);
	}
	//	SetBoxPush(false);
	//AddMovementInput(CharacterVector, Value);
}

void ABDPBox::MoveBoxRight(float& Value, FVector& CharacterVector)
{
	if (GetBoxPush())
	{
	FVector Location = GetActorLocation();
	SetActorLocation(Location + CharacterVector * Value*0.5,true);
	}
	//SetBoxPush(false);
}

