// BoxDPTestTask


#include "Player/BDPCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "BoxActor.h"
#include "GameFramework/SpringArmComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"

// Sets default values
ABDPCharacter::ABDPCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	//RootComponent = SceneComponent;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
	SpringArmComponent->SetupAttachment(GetRootComponent());
	SpringArmComponent->bUsePawnControlRotation = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(SpringArmComponent);

	PhysicsHandle = CreateDefaultSubobject<UPhysicsHandleComponent>("PhysicsHandleComponent");
}

// Called when the game starts or when spawned
void ABDPCharacter::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABDPCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//That part moved to BP
	//if (bIsPushing)
	//{
	//	FVector CharLocation = this->GetActorLocation();  
	//	FVector CharForwardVector = this->GetActorForwardVector() * 200.0f;
	//	FVector NewLocation = CharLocation + CharForwardVector;

	//	PhysicsHandle->SetTargetLocation(NewLocation);
	//}
}

// Called to bind functionality to input
void ABDPCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABDPCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABDPCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &ABDPCharacter::LookUp);
	PlayerInputComponent->BindAxis("Turn", this, &ABDPCharacter::Turn);

	PlayerInputComponent->BindAction("DrugAndPush", IE_Pressed, this, &ABDPCharacter::Push);
	PlayerInputComponent->BindAction("DrugAndPush", IE_Released, this, &ABDPCharacter::StopPush);
}

void ABDPCharacter::MoveForward(float Value)
{
	FVector ForwardVector = GetActorForwardVector();

	if (bIsPushing)
	{
		AddMovementInput(GetActorForwardVector() / 2, Value);
	}
	else
	{
		AddMovementInput(GetActorForwardVector(), Value);

	}

}

void ABDPCharacter::MoveRight(float Value)
{

	FVector RightVector = GetActorRightVector();

	if (bIsPushing)
	{
		//AddMovementInput(GetActorRightVector() / 2, Value);
	}
	else
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ABDPCharacter::LookUp(float Value)
{
	if (!bIsPushing)
		AddControllerPitchInput(Value);
}

void ABDPCharacter::Turn(float Value)
{
	if (!bIsPushing)
		AddControllerYawInput(Value);
}

void ABDPCharacter::Push()
{
	FHitResult OutHit;
	FVector CharLocation = this->GetActorLocation();  
	FVector CharForwardVector = this->GetActorForwardVector() * 50.0f;
	FVector EndLocation = CharLocation + CharForwardVector;

	bool bHit = GetWorld()->LineTraceSingleByChannel(OutHit, CharLocation, EndLocation, ECC_Visibility);

	if (bHit)
	{
		ABoxActor* Box = Cast<ABoxActor>(OutHit.GetActor());

		if (Box)
		{
			FVector ComponentLocation = OutHit.GetComponent()->GetComponentLocation();

			PhysicsHandle->GrabComponentAtLocationWithRotation(OutHit.GetComponent(), FName("None"), ComponentLocation, FRotator(0.0f, 0.0f, 0.0f));

			bIsPushing = true;

			PlayAnimMontage(PushMontage);

			UE_LOG(LogTemp, Warning, TEXT("U R Pushing"));
		}
	}

}

void ABDPCharacter::StopPush()
{
	if (bIsPushing)
	{
		PhysicsHandle->ReleaseComponent();

		bIsPushing = false;
		StopAnimMontage(PushMontage);
	
		UE_LOG(LogTemp, Warning, TEXT("Now U R not Pushing"));
	}
}

