// Copyright Epic Games, Inc. All Rights Reserved.

#include "BoxDPGameMode.h"
#include "Player/BDPCharacter.h"
#include "Player/BDPPlayerController.h"
#include "UObject/ConstructorHelpers.h"

ABoxDPGameMode::ABoxDPGameMode()
{
	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	//if (PlayerPawnBPClass.Class != NULL)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}

	DefaultPawnClass = ABDPCharacter::StaticClass();
	PlayerControllerClass = ABDPPlayerController::StaticClass();

}
