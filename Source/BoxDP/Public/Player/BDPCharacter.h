// BoxDPTestTask

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BDPCharacter.generated.h"



class UCameraComponent;
class USpringArmComponent;
class UPhysicsHandleComponent;


UCLASS()
class BOXDP_API ABDPCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABDPCharacter();

protected:


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UCameraComponent* CameraComponent;
	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	//	USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USpringArmComponent* SpringArmComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UPhysicsHandleComponent* PhysicsHandle;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
		UAnimMontage* PushMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	bool bIsPushing = false;

	UFUNCTION(BlueprintCallable)
		bool GetIsPushing() { return bIsPushing;}

	UFUNCTION(BlueprintCallable)
	void StopPush();
private:

	void MoveForward(float Value);
	void MoveRight(float Value);

	void LookUp(float Value);
	void Turn(float Value);

	void Push();
};
