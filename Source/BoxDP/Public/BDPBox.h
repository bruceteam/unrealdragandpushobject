// BoxDPTestTask

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BDPBox.generated.h"

UCLASS()
class BOXDP_API ABDPBox : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABDPBox();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UStaticMeshComponent* StaticMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	bool CanPush();

	bool BoxPush = false;

	bool GetBoxPush() { return BoxPush; }

	void SetBoxPush(bool value) { BoxPush = value; }

	//auto GetBoxActor() { return this; }

	UFUNCTION()
	void MoveBoxForward(float &Value, FVector& CharacterVector);
	UFUNCTION()
	void MoveBoxRight(float &Value, FVector& CharacterVector);
};
