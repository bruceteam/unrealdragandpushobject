// BoxDPTestTask

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Player/BDPCharacter.h"
#include "BoxActor.generated.h"

UCLASS()
class BOXDP_API ABoxActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoxActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UStaticMeshComponent* StaticMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
